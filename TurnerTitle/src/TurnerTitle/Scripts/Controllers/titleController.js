﻿(function () {
    'use strict';

    TitlesController.$inject = ['$scope', '$http'];

    angular.module('titlesApp').controller('TitlesController', TitlesController)
    .controller('TitleController', TitleController);


    function TitlesController($scope, $http) {
        //$scope.test = 'test';
        //$scope.names = [
        //    { name: 'Jani', country: 'Norway' },
        //    { name: 'Hege', country: 'Sweden' },
        //    { name: 'Kai', country: 'Denmark' }
        //];

        //alert('test');
        var url = "http://localhost:53550/api/title/";

        $scope.SearchName = '';
        $scope.Title = {};
        $scope.Titles = {};
        

        $scope.resetDetails = function () {
            $scope.Title = {};
        }

        
        $scope.getTitle = function ($event) {
            //alert($event.target.id);           

            $http.get(url.concat($event.target.id)).then(function (response) {
                $scope.Title = response.data;
                                
            });
        }

        $http.get(url).then(function (response) {
            $scope.Titles = response.data;
        });
            }

  

    //angular
    //    .module('titlesApp')
    //    .controller('TitleListController', TitleListController)
    //    .controller('TitleDetailController', TitleDetailController)
        

    ///* Title List Controller  */
    //TitleListController.$inject = ['$scope', '$routeParams', 'TitlesService', 'SearchTitleProvider'];

    //function TitleListController($scope, $routeParams, TitlesService, SearchTitleProvider) {
    //    $scope.SearchTitle = SearchTitleProvider.SearchTitle;

    //    $scope.Titles = TitlesService.Titles({ id: $routeParams.name });
        
    //}

    ///* Title Detail Controller */
    //TitleDetailController.$inject = ['$scope', '$routeParams',   'TitleService'];

    //function TitleDetailController($scope, $routeParams, TitleService) {
    //    $scope.Title = TitlesService.Title({ id: $routeParams.id });       
    //}

    


})();
