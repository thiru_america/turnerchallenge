﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using TurnerTitles.Repositories;
using TurnerTitles.Models;

namespace TurnerTitles.Controllers
{
    [Route("api/[controller]")]
    public class TitleController : Controller
    {
        [FromServices]
        public ITitleRepository Repository { get; set; }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var title = Repository.FindTitle(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            else {
                return new ObjectResult(title);
            }
        }

        [HttpGet("Name/{name}")]
        public IEnumerable<Title> GetByName(string name)
        {
            return  Repository.GetTitles(name);
        }

        [HttpGet()]
        public IEnumerable<Title> GetAll()
        {
            return Repository.GetAll();
        }
        
    }
}
