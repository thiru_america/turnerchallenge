﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using TurnerTitles.Models;

namespace TurnerTitles.Repositories
{

    public class TitleRepository : ITitleRepository
    {        
        private AppDbContext DbContext;

        public TitleRepository(AppDbContext dbcontext) {
            DbContext = dbcontext;
        }

        public Title FindTitle(int id)
        {
            Title title;
            title = DbContext.Titles.Where(t => t.TitleId == id).FirstOrDefault();

            return title;
        }
        public IEnumerable<Title> GetTitles(string name)
        {
            return DbContext.Titles.Where(t => t.TitleName.Contains(name));
        }
        public IEnumerable<Title> GetAll()
        {
            return DbContext.Titles.AsEnumerable<Title>();
        }

    }
}
