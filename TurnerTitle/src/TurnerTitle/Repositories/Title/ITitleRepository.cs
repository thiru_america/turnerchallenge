﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TurnerTitles.Models;

namespace TurnerTitles.Repositories
{
    public interface ITitleRepository
    {        
        Title FindTitle(int id);
        IEnumerable<Title> GetAll();
        IEnumerable<Title> GetTitles(string name);        
    }
}
