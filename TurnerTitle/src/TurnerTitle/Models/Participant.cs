using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class Participant
    {
        public Participant()
        {
            TitleParticipant = new HashSet<TitleParticipant>();
        }

        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string ParticipantType { get; set; }

        [InverseProperty("Participant")]
        public virtual ICollection<TitleParticipant> TitleParticipant { get; set; }
    }
}
