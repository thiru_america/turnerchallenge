using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class Title
    {
        public Title()
        {
            Award = new HashSet<Award>();
            OtherName = new HashSet<OtherName>();
            StoryLine = new HashSet<StoryLine>();
            TitleGenre = new HashSet<TitleGenre>();
            TitleParticipant = new HashSet<TitleParticipant>();
        }

        public int TitleId { get; set; }
        public DateTime? ProcessedDateTimeUTC { get; set; }
        public int? ReleaseYear { get; set; }
        [MaxLength(100)]
        public string TitleName { get; set; }
        [MaxLength(100)]
        public string TitleNameSortable { get; set; }
        public int? TitleTypeId { get; set; }

        [InverseProperty("Title")]
        public virtual ICollection<Award> Award { get; set; }
        [InverseProperty("Title")]
        public virtual ICollection<OtherName> OtherName { get; set; }
        [InverseProperty("Title")]
        public virtual ICollection<StoryLine> StoryLine { get; set; }
        [InverseProperty("Title")]
        public virtual ICollection<TitleGenre> TitleGenre { get; set; }
        [InverseProperty("Title")]
        public virtual ICollection<TitleParticipant> TitleParticipant { get; set; }
    }
}
