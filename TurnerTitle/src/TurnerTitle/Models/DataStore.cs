using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;

namespace TurnerTitles.Models
{
    public partial class DataStore : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"data source=(localdb)\MSSQLLocalDB;initial catalog=Titles;Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Participant>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<StoryLine>(entity =>
            {
                entity.Property(e => e.Description).HasColumnType("ntext");
            });

            modelBuilder.Entity<Title>(entity =>
            {
                entity.Property(e => e.TitleId).ValueGeneratedNever();

                entity.Property(e => e.ProcessedDateTimeUTC).HasColumnType("datetime");
            });
        }

        public virtual DbSet<Award> Award { get; set; }
        public virtual DbSet<Genre> Genre { get; set; }
        public virtual DbSet<OtherName> OtherName { get; set; }
        public virtual DbSet<Participant> Participant { get; set; }
        public virtual DbSet<StoryLine> StoryLine { get; set; }
        public virtual DbSet<Title> Title { get; set; }
        public virtual DbSet<TitleGenre> TitleGenre { get; set; }
        public virtual DbSet<TitleParticipant> TitleParticipant { get; set; }
    }
}