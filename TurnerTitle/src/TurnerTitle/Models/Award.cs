using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class Award
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Awards { get; set; }
        [MaxLength(100)]
        public string AwardCompany { get; set; }
        public bool? AwardWon { get; set; }
        public int? AwardYear { get; set; }
        public int TitleId { get; set; }

        [ForeignKey("TitleId")]
        [InverseProperty("Award")]
        public virtual Title Title { get; set; }
    }
}
