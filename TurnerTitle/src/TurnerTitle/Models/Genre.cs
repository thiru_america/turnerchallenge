using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class Genre
    {
        public Genre()
        {
            TitleGenre = new HashSet<TitleGenre>();
        }

        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }

        [InverseProperty("Genre")]
        public virtual ICollection<TitleGenre> TitleGenre { get; set; }
    }
}
