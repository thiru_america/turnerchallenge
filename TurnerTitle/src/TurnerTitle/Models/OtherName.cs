using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class OtherName
    {
        public int Id { get; set; }
        public int? TitleId { get; set; }
        [MaxLength(100)]
        public string TitleName { get; set; }
        [MaxLength(100)]
        public string TitleNameLanguage { get; set; }
        [MaxLength(100)]
        public string TitleNameSortable { get; set; }
        [MaxLength(100)]
        public string TitleNameType { get; set; }

        [ForeignKey("TitleId")]
        [InverseProperty("OtherName")]
        public virtual Title Title { get; set; }
    }
}
