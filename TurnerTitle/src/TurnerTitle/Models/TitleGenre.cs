using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class TitleGenre
    {
        public int Id { get; set; }
        public int GenreId { get; set; }
        public int TitleId { get; set; }

        [ForeignKey("GenreId")]
        [InverseProperty("TitleGenre")]
        public virtual Genre Genre { get; set; }
        [ForeignKey("TitleId")]
        [InverseProperty("TitleGenre")]
        public virtual Title Title { get; set; }
    }
}
