﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurnerTitles.Models
{
    public class AppDbContext:DbContext
    {
        public DbSet<Title> Titles { get; set; }        
    }
}
