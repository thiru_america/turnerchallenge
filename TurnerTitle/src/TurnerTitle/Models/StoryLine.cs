using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class StoryLine
    {
        public int Id { get; set; }
        public string Description { get; set; }
        [MaxLength(100)]
        public string Language { get; set; }
        public int TitleId { get; set; }
        [MaxLength(100)]
        public string Type { get; set; }

        [ForeignKey("TitleId")]
        [InverseProperty("StoryLine")]
        public virtual Title Title { get; set; }
    }
}
