using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TurnerTitles.Models
{
    public partial class TitleParticipant
    {
        public int Id { get; set; }
        public bool IsKey { get; set; }
        public bool IsOnScreen { get; set; }
        public int ParticipantId { get; set; }
        [MaxLength(100)]
        public string RoleType { get; set; }
        public int TitleId { get; set; }

        [ForeignKey("ParticipantId")]
        [InverseProperty("TitleParticipant")]
        public virtual Participant Participant { get; set; }
        [ForeignKey("TitleId")]
        [InverseProperty("TitleParticipant")]
        public virtual Title Title { get; set; }
    }
}
